<?php

namespace Romain\Tools\Pattern;

/**
 * Singleton trait
 */
trait SingletonTrait {
	
	protected static $_instance = null;
	
	/**
	 * Get a default instance
	 * @return 
	 */
	public static function instance($configuration) {
		if (self::$_instance === null) {
			self::$_instance = new self($configuration);
		}
		return self::$_instance;
	}
}